#!/usr/bin/env python3

import base64

# Known value, too long to type again and again later so we use short variable name :-)
message = "This is a very long bit of text that we encrypt and now it will be even longer and harder to work with in the application"

message_bytes = message.encode('ascii')
base64_bytes = base64.b64encode(message_bytes)
base64_message = base64_bytes.decode('ascii')
print("Encrpyted string: \n\t" + base64_message)

base64_bytes = base64_message.encode('ascii')
message_bytes = base64.b64decode(base64_bytes)
message = message_bytes.decode('ascii')

print("Original message:\n\t" + message)
print("and again...\n\t" + message)

# Getting information that is unknown to us (like the number of lines in a file) but we need to use that to inform how the program acts later
# Opening a file
file = open("myfile.txt","r")

# Reading from file
content = file.read()
lineList = content.split("\n")
lines = len(lineList)

print(f"\n\nThis is the number of lines in the file:\n\t{lines}")
